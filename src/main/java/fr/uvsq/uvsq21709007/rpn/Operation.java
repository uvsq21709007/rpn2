/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.uvsq21709007.rpn;

/**
 *
 * @author user
 */
public enum Operation {
    DIV("/") {
        @Override
        public double eval(double op1, double op2) throws ArithmeticException {
            if (op2 != 0) {
                return op1 / op2;
            } else {
                throw new ArithmeticException("Erreur Divizion par zero");
            }
        }
    },
    PLUS("+") {
        @Override
        public double eval(double op1, double op2
        ) {
            return op1 + op2;
        }
    },
    MULT("*") {
        @Override
        public double eval(double op1, double op2
        ) {
            return op1 * op2;
        }
    },
    MOINS("-") {
        @Override
        public double eval(double op1, double op2
        ) {
            return op1 - op2;
        }
    };

    private char symbole;
    int Max_value = 2000;
    int Min_value = -2000;

    String symbol;

    Operation(String symbol) {
        this.symbol = symbol;
    }

    public abstract double eval(double op1, double op2);
}
