
package fr.uvsq.uvsq21709007.rpn;

import java.util.Scanner;


public class SaisieRPN {

    String s;
    Operation operation;
    Scanner scanner = new Scanner(System.in);

    public void interactionUser() {
        MoteurRPN m = new MoteurRPN();
        System.out.println("Saisissez les nombres et les symboles suivi de la touche Entrée");
        s = scanner.nextLine();
        while (!"exit".equals(s)) {
            if (isDouble(s)) {
                m.getPile().push(Double.parseDouble(s));
                m.affichePile();
            } else {
                switch (s) {
                    case "+":
                        m.calculeOperation(Operation.PLUS);
                        m.affichePile();
                        break;
                    case "-":
                        m.calculeOperation(Operation.MOINS);
                        m.affichePile();
                        break;
                    case "*":
                        m.calculeOperation(Operation.MULT);
                        m.affichePile();
                        break;
                    case "/":
                        m.calculeOperation(Operation.DIV);
                        m.affichePile();
                        break;
                    default:
                        System.err.println("Erreur !!!! entrez soit un nombre, soit +,-,* ou /");
                        break;
                }
            }
            s = scanner.nextLine();
        }
    }

    private boolean isDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
