/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uvsq.uvsq21709007.rpn;

import java.util.Stack;

/**
 *
 * @author user
 */
public class MoteurRPN {

    Stack<Double> pile = new Stack<>();
    Double resultat = new Double("0");

    public Stack<Double> getPile() {
        return pile;
    }

    public void setPile(Stack<Double> pile) {
        this.pile = pile;
    }

    public void calculeOperation(Operation operation) {
        if (pile.size() > 1) {
            switch (operation) {
                case PLUS:
                    pile.push(Operation.PLUS.eval(pile.pop(), pile.pop()));

                    break;
                case MOINS:
                    pile.push(Operation.MOINS.eval(pile.pop(), pile.pop()));
                    break;
                case MULT:
                    pile.push(Operation.MULT.eval(pile.pop(), pile.pop()));
                    break;
                case DIV:
                    try {
                        pile.push(Operation.DIV.eval(pile.pop(), pile.pop()));
                    } catch (ArithmeticException e) {
                        System.out.println(e.getMessage());
                    }

                    //System.out.println("le resultat"+pile.peek());
                    break;
            }
        } else {
            System.out.println("Erreur : il faut au moins deux operandes!");
        }
    }

    public void affichePile() {
        System.out.println("contenu Pile = " + pile.toString());
    }
}
