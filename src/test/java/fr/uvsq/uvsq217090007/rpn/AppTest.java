package fr.uvsq.uvsq217090007.rpn;

import fr.uvsq.uvsq21709007.rpn.MoteurRPN;
import fr.uvsq.uvsq21709007.rpn.Operation;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        MoteurRPN moteur = new MoteurRPN();

        moteur.getPile().push(new Double("2"));
        moteur.getPile().push(new Double("8"));
        moteur.calculeOperation(Operation.PLUS);
        assertEquals(moteur.getPile().pop(), new Double("10"));
        
        moteur.getPile().push(new Double("2"));
        moteur.getPile().push(new Double("8"));
        moteur.calculeOperation(Operation.MOINS);
        assertEquals(moteur.getPile().pop(), new Double("6"));
        
        moteur.getPile().push(new Double("2"));
        moteur.getPile().push(new Double("8"));
        moteur.calculeOperation(Operation.DIV);
        assertEquals(moteur.getPile().pop(), new Double("4"));
        
        
        moteur.getPile().push(new Double("2"));
        moteur.getPile().push(new Double("8"));
        moteur.calculeOperation(Operation.MULT);
        assertEquals(moteur.getPile().pop(), new Double("16"));
    }
}
